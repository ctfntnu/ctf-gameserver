#!/ usr/bin/bash
port="$1"
/usr/bin/systemctl enable ctf-submission@"$port"
/usr/lib/systemd/systemd --system --unit=multi-user.target
