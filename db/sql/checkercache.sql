CREATE USER ctf_cache;
ALTER USER ctf_cache WITH PASSWORD 'ctf'; 
CREATE DATABASE checkercache;
GRANT ALL PRIVILEGES ON DATABASE checkercache TO ctf_cache;

CREATE TABLE checkercache (
  team_id INTEGER,
  service_id INTEGER,
  identifier CHARACTER VARYING (128),
  data BYTEA
);
