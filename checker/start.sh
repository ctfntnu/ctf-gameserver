#! /usr/bin/bash

export PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/app/
export PYTHONPATH=/usr/local/lib/python3.5:/etc/ctf_gameserver/checker/
for checker in /etc/ctf_gameserver/checker/*checker.conf; do
    cp "$checker" /etc/ctf_gameserver/
done

for checker in /etc/ctf_gameserver/*checker.conf; do
    echo "[+] Starting checker ${checker%.conf}"
    python /app/ctf-checkermaster @/etc/ctf_gameserver/checkermaster.conf @"$checker" >> /var/log/app.log  &
done

tail -f /var/log/app.log
