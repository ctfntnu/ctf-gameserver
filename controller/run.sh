#!/usr/bin/bash
export PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
echo "[+] Execute controller"
python /app/ctf-controller @/etc/ctf_gameserver/controller.conf 
echo "[+] Execute scorting"
python /app/ctf-scoring @/etc/ctf_gameserver/scoring.conf 
