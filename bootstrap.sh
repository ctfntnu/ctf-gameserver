#! /usr/bin/bash
# docker-compose up -d --no-deps --build <service_name> -> update container
#https://medium.com/@nirgn/load-balancing-applications-with-haproxy-and-docker-d719b7c5b231
EXEC="docker-compose exec -T"
DJANGO_MANAGE="$EXEC web python manage.py"


function create_admin {
        user="$1"
        password="$2"
        $DJANGO_MANAGE createsuperuser2 --username $user --password $password --noinput --email 'admin@ctf.local'
}

function create_db {
        $DJANGO_MANAGE migrate auth
        $DJANGO_MANAGE migrate
        $EXEC db bash /scoring.sh
}

function check_admin {
        user="$1"
        $EXEC db psql -h localhost -d ctf -U ctf -p 5432 -c "SELECT * FROM auth_user" | grep -qw $user
        return $?
}

function check_db {
        $EXEC db psql -h localhost -d ctf -U ctf -p 5432 -c "SELECT * FROM scoring_scoreboard" | grep -q "team_id | service_id | attack | bonus | defense | sla | total"
        return $?
}



action="$1"
user="$2"
password="$3"

case $action in
        create_db)
                create_db
                ;;
        check_db)
                exit $(check_db)
                ;;
        create_admin)
                create_admin $user $password
                ;;
        check_admin)
                exit $(check_admin $user)
                ;;
        *)
                echo "$action not implemented"
                ;;
esac
